# Changelog

All notable changes to this project will be documented in this file..

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

If we have a Breaking Change, we have to add this new tag (**_ Breaking change _**) before each breaking change line:

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [2.0.0] - 2024-04-25

### Added

- Added `argocd-release.gitlab-ci.yml` file to update the ArgoCD's application version in GitLab CI. It updates the targetRevision Tag in the ArgoCD application manifest file.
- Added `helm-lint.gitlab-ci.yml` file to lint helm charts in GitLab CI.
- Added `helm-release.gitlab-ci.yml` file to release helm charts in GitLab CI. It increments the used docker image version in the `values.yaml` and increments the helm chart version in the `Chart.yaml` file.
- Added `maven-compile.gitlab-ci.yml` file to compile maven projects in GitLab CI.
- Added `maven-release.gitlab-ci.yml` file to release maven projects in GitLab CI. It increments the version in the pom.xml file and creates a tag in the git repository.

### Changed

- Renamed template `docker-build-kaniko-template` to `docker-build-kaniko` in `docker-build-kaniko.gitlab-ci.yml`.

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0] - 2024-04-03

### Added

- Added `docker-build-kaniko.gitlab-ci.yml` file to build docker images using kaniko in GitLab CI pipeline.
- Added `Dockerfile` under `images/maven` directory which is used to run the maven build in the GitLab CI pipeline.
- Added `Dockerfile` under `images/util` directory which is used to run utility commands in the GitLab CI pipeline.
- Added `.gitlab-ci.yml` file to run the GitLab CI pipeline for the project.

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...
